import requests
import pandas as pd
import numpy as np
import json

URL = 'https://api.k8s.dev.easymedstat.com'

def send_stats_datasets(dataframe):
    variables = []
    for column in dataframe.keys():
        if type(dataframe[column]) is np.ndarray:
            variables.append({
                "id": column,
                "data": dataframe[column].tolist()
            })
        elif type(dataframe[column]) is list:
            variables.append({"id": column, "data": dataframe[column]})
        elif type(dataframe[column]) is pd.core.series.Series:
            variables.append({
                "id": column,
                "data": dataframe[column].values.tolist()
            })
        else:
            raise Exception("Unkown column type")

    payload = {
        "id":
        "test1",
        "access_token":
        "test1",
        "premium":
        True,
        "variables": [{
            "id": "firstvar",
            "name": "firstvar",
            "groups": variables,
        }],
    }
    headers = {"Content-Type": "application/json"}
    full_url = URL + "/datasets/"
    dump = json.dumps(payload)
    response = requests.request("POST", full_url, headers=headers, data=dump)
    return response.text, response.status_code


def send_data(file, data_id=None, access_token=None):
    df = pd.read_csv("./tests/test_data/" + file, sep=",", index_col=0)
    full_url = URL + "/stats/multivariate/data"
    headers = {"Content-Type": "application/json"}
    variables = [{
        "id": name,
        "name": name,
        "groups": [{
            "id": name,
            "data": list(val.values.tolist())
        }],
    } for name, val in zip(df.columns, [df[c] for c in df.columns])]

    payload = {
        "id": "test1",
        "access_token": "test1",
        "premium": True,
        "variables": variables,
    }
    if data_id is not None:
        payload["id"] = data_id
    if access_token is not None:
        payload["access_token"] = access_token
    dump = json.dumps(payload)
    response = requests.request("POST", full_url, headers=headers, data=dump)
    return response.text, response.status_code


def send_model(data_id=None, model_id=None, target=None, explanatory=None):
    full_url = URL + "/stats/multivariate/SetModel"

    headers = {"Content-Type": "application/json"}
    payload = {
        "target": {
            "name": "bmi",
            "transform": {
                "type": "Cutoff low",
                "opts": {
                    "low": 30
                },
            }
        },
        "explanatory": [{
            "name": "sex",
            "transform": {
                "type": "None",
                "opts": {
                    "low": 30
                },
            }
        }]
    }
    if data_id is not None:
        payload["data_id"] = data_id
    if model_id is not None:
        payload["id"] = model_id
    if target is not None:
        payload["target"] = {
            "name": target[0],
            "transform": {
                "type": target[1],
                "opts": target[2]
            }
        }
    if explanatory is not None:
        constructor = []
        for variable in explanatory:
            constructor.append({
                "name": variable[0],
                "transform": {
                    "type": variable[1],
                    "opts": variable[2]
                }
            })
        payload["explanatory"] = constructor
    dump = json.dumps(payload)
    response = requests.request("POST", full_url, headers=headers, data=dump)
    if response.status_code != 200:
        return response, response.status_code
    else:
        return response.json(), response.status_code


def get_model(model_id):
    response = requests.request("GET",
                                URL + "/stats/multivariate/model/" + model_id)
    if response.status_code != 200:

        return response, response.status_code
    else:
        return response.json(), response.status_code


def get_transformed(model_id):
    response = requests.request(
        "GET", URL + "/stats/multivariate/transformed/" + model_id)
    if response.status_code != 200:
        return response, response.status_code
    else:
        return response.json(), response.status_code


def get_collinearity(model_id):
    response = requests.request(
        "GET", URL + "/stats/multivariate/collinearity/" + model_id)
    if response.status_code != 200:
        return response, response.status_code
    else:
        return response.json(), response.status_code


def get_stats(data_id, name, params=None):
    payload = {
        "name": name,
        "dataset_id": data_id,
        "dataset_access_token": data_id,
    }
    if params is not None:
        payload["parameters"] = params
    headers = {"Content-Type": "application/json"}
    full_url = URL + "/stats/test/" + name
    dump = json.dumps(payload)
    response = requests.request("POST", full_url, headers=headers, data=dump)
    return response, response.status_code


if __name__ == "__main__":
    # data = send_data("insurance.csv")[0]
    #
    # print("data received and saved using data id :: {}".format(data))
    # resp, _ = send_model(data_id=data)
    # resp, _ = send_model(data_id=data,
    #                      model_id=resp["model_id"],
    #                      target=["bmi", "Cutoff low", {
    #                          "low": 30
    #                      }],
    #                      explanatory=[["sex", "None", {}],
    #                                   ["region", "None", {}]])
    # print(str(resp).replace(",", ",\n"))
    #
    # transformed, _ = get_transformed(resp["model_id"])
    # print(pd.read_json(json.dumps(transformed)))
    #
    # transformed, _ = get_collinearity(resp["model_id"])
    # print(transformed)

    data_id, status = send_stats_datasets(
        pd.DataFrame({
            "c1": np.random.normal(size=100) + 100,
            "c2": np.random.normal(size=100)
        }))
    assert status == 201
    resp, code = get_stats(data_id, "kruskal", {"alternative": "None"})
    print(resp)
    print(code)
